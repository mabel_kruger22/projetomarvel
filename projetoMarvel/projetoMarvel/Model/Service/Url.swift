//
//  Url.swift
//  projetoMarvel
//
//  Created by C94291a on 29/11/21.
//

import Foundation

class Url{
    var offSet: Int = 0
    var getMd5 = Md5()
    
    // API
    let baseURL = "http://gateway.marvel.com"
    let path = "v1/public/characters"

    // Auth
    let publicKey = Bundle.main.object(forInfoDictionaryKey: "publicKey") as! String
    let privateKey = Bundle.main.object(forInfoDictionaryKey: "privateKey") as! String

    // Timestamp
    let ts = Int(Date().timeIntervalSince1970)
    
    func requestURL() -> String{
        let content = String(ts) + privateKey + publicKey
        let hash = getMd5.MD5(string: content)
        // URL
        let url = baseURL + "/" + path + "?" + "&offset=\(self.offSet)" + "&ts=\(ts)" + "&apikey=\(publicKey)" + "&hash=\(hash)"
        return url
    }
    
    func requestAPI(name: String) -> String {
        let content = String(ts) + privateKey + publicKey
        let hash = getMd5.MD5(string: content)
        // URL
        let url = baseURL + "/" + path + "?" + "nameStartsWith=\(name)" + "&ts=\(ts)" + "&apikey=\(publicKey)" + "&hash=\(hash)"
        return url
    }
}
