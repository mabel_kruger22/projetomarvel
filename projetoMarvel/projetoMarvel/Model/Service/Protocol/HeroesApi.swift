//
//  HttpGet.swift
//  projetoMarvel
//
//  Created by C94291a on 03/12/21.
//

import Foundation

protocol HeroesApi {
    func getHeroes(url: String, completion: @escaping (Response?, Error?) -> Void)
    var getUrl: Url {get set}
    var pagination: Bool {get set}
}
