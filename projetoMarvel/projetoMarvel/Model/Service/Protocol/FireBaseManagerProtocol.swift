//
//  FireBaseControllerProtocol.swift
//  projetoMarvel
//
//  Created by C94291a on 31/01/22.
//

import Foundation

protocol FireBaseManagerProtocol{
    func sendScreenDataToGA(screenName: String, screenClass: String)
    func changeTheName(nameHero: String)
    func setUserIDAndUserProperty(propertyUser food: String)
}
