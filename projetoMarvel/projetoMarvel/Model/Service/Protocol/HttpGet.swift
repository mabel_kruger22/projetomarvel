//
//  HttpGet.swift
//  projetoMarvel
//
//  Created by C94291a on 07/12/21.
//

import Foundation

protocol HttpGet{
    func get(url : String, completion: @escaping (Response?, Error?) -> Void)
}
