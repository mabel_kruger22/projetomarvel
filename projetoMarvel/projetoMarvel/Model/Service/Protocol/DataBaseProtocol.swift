//
//  DataBaseProtocol.swift
//  projetoMarvel
//
//  Created by C94291a on 18/01/22.
//

import Foundation

protocol DataBaseProtocol {
    static func getdataFromCoreData() -> [Characters]
    static func saveCharacterInTheDataBase(_ models: [Character])
    static func checkIfHeroExist(nameHero: String) -> Bool
}
