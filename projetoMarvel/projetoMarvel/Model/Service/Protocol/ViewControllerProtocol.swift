//
//  ViewControllerProtocol.swift
//  projetoMarvel
//
//  Created by C94291a on 14/01/22.
//

import Foundation

protocol ViewControllerProtocol{
    var  reloadDataCount: Int {get set}
    var  alertInternetIsNotConnectCount: Int {get set}
    func reloadData()
    func setupLoading()
    func alertInternetIsNotConect()
}
