//
//  ValidateInternetProtocol.swift
//  projetoMarvel
//
//  Created by C94291a on 19/01/22.
//

import Foundation

protocol ValidateInternetProtocol{
    func isConnectedToNetwork() -> Bool
}
