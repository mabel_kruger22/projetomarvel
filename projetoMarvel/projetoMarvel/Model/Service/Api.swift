//
//  Api.swift
//  projetoMarvel
//
//  Created by C94291a on 26/11/21.
//

import Foundation


//aqui estou fazendo uma struct que pega o protocol para fazer a injecao de depencia da api, mas sem uma url concreta.
struct Api{
    let getProtocol: HttpGet
    func getHerois(url : String, completion: @escaping (Response?, Error?) -> Void){
        getProtocol.get(url: url) { data, error in
            if let result = data {
                completion(result, nil)
            } else{
                completion(nil, ErrorAPI.emptyArray)
            }
        }
    }
}

// aqui estou fazendo a chamada real da api, junto com o url da api desejada.
class RealApi: HeroesApi{
    var pagination = false
    var getUrl = Url()
    var statusCode: Int = 0
    
    //MARK: pega as informacoes da API
     func getHeroes(url: String, completion: @escaping (Response?, Error?) -> Void){
        if let url = URL(string: url){
            self.pagination = true
            URLSession.shared.dataTask(with: url) {data, responseURL, error in
                self.pagination = false
                if let responseData = data{
                    do{
                        let posts =  try JSONDecoder().decode(Response.self, from: responseData)
                        self.getUrl.offSet += 20
                        if let response = responseURL as? HTTPURLResponse{
                            self.statusCode  = response.statusCode
                            print(self.statusCode)
                        }
                        DispatchQueue.main.async {
                            completion(posts, nil)
                        }
                        
                    }catch let error{
                        print(error)
                        DispatchQueue.main.async {
                            completion(nil,  ErrorAPI.emptyArray)
                        }
                    }
                }
            }.resume()
        }
    }
}
