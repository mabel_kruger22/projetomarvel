//
//  Heroi.swift
//  projetoMarvel
//
//  Created by C94291a on 19/11/21.
//

import Foundation

struct Character: Codable{
    let name: String
    let thumbnail: Image
    let comics: Comics?
    let stories: StoryList?
}
