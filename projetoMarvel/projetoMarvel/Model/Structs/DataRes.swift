//
//  DataResult.swift
//  projetoMarvel
//
//  Created by C94291a on 14/02/22.
//

import Foundation

struct DataRes: Codable {
    let results: [Character]
}
