//
//  ComicList.swift
//  projetoMarvel
//
//  Created by C94291a on 14/02/22.
//

import Foundation

struct ComicList: Codable{
    let resourceURI: String?
    let name: String?
}
