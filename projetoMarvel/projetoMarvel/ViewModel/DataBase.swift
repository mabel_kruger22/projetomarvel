//
//  DataBaseController.swift
//  projetoMarvel
//
//  Created by C94291a on 13/12/21.
//

import CoreData

class DataBase : DataBaseProtocol{
    
    // MARK:
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "projetoMarvel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    static func saveContext(completion: @escaping (Result<Void, Error>) -> Void) {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                completion(Result.success(()))
            } catch let error {
                completion(Result.failure(error))
            }
        }
    }
    
    static func getdataFromCoreData() -> [Characters]{
        
        var dataSavedCharacter: [Characters] = []
        do {
            dataSavedCharacter = try persistentContainer.viewContext.fetch(Characters.fetchRequest())
        } catch {
            print("It's not possible get Data from CoreData")
        }
        print("AEEEE \(dataSavedCharacter.count)")
        return dataSavedCharacter
    }
    
    static func saveCharacterInTheDataBase(_ models: [Character]) {
        let save = persistentContainer.viewContext

        for characters in models{
            if !checkIfHeroExist(nameHero: characters.name ){
                persistentContainer.viewContext.processPendingChanges()
                let dataCharacter = Characters(context: save)
                
                dataCharacter.nameHero = characters.name
                dataCharacter.imageHeroPath = characters.thumbnail.path
                dataCharacter.imageHeroExtension = characters.thumbnail.`extension`
                
                guard let charactersStories = characters.stories?.items else {return}
                if charactersStories.count > 0{
                    var storyData: [Datas] = []
                    for itens in charactersStories{
                        if itens.name != nil {
                            persistentContainer.viewContext.processPendingChanges()
                            let data = Datas(context: save)
                            data.name = itens.name
                            data.resource = itens.resourceURI
                            
                            storyData.append(data)
                        }
                    }
                    let storySet = Set(storyData) as NSSet
                    dataCharacter.storyHero = storySet
                }
                
                guard let charactersComics = characters.comics?.items else {return}
                if charactersComics.count > 0 {
                    var comicsData: [Datas] = []
                    for itens in charactersComics{
                        if itens.name != nil{
                            persistentContainer.viewContext.processPendingChanges()
                            let data = Datas(context: save)
                            data.name = itens.name
                            data.resource = itens.resourceURI
                            
                            comicsData.append(data)
                        }
                    }
                    let comicsSet = Set(comicsData) as NSSet
                    dataCharacter.comicHero = comicsSet
                }
            }
            saveContext { result in
                switch result {
                case .success:
                    print("salvando")
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    static func checkIfHeroExist(nameHero: String) -> Bool {

        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Characters")
        fetchRequest.fetchLimit =  1
        fetchRequest.predicate = NSPredicate(format: "nameHero =%@", nameHero)

        do {
            let count = try managedContext.count(for: fetchRequest)
            if count > 0 {
                return true
            }else {
                return false
            }
        }catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return false
        }
    }
}
