//
//  ViewController.swift
//  projetoMarvel
//
//  Created by C94291a on 19/11/21.
//

import Lottie
import CoreData

class ViewModel{
    
    //MARK: variaveis
    var arrayHeroesVertical: [Character] = []
    var arrayHeroesHorizontal: [Character] = []
    let reuseIdentifier1  = "cellHorizontal"
    let reuseIdentifier2  = "cellVertical"
    
    var animation = Animation()
    
    var filter = false
    var isHidden = false
    var isConnect = false
    var error: Error?
    
    var apiProtocol: HeroesApi
    var dataBaseProtocol: DataBaseProtocol
    var view: ViewControllerProtocol
    var internetProtocol: ValidateInternetProtocol
    var fireBaseProtocol: FireBaseManagerProtocol
    
    init(view: ViewControllerProtocol, dataBaseProtocol: DataBaseProtocol, apiProtocol: HeroesApi, validateInternetProtocol: ValidateInternetProtocol, fireBaseProtocol: FireBaseManagerProtocol) {
        self.view = view
        self.dataBaseProtocol = dataBaseProtocol
        self.apiProtocol = apiProtocol
        self.internetProtocol = validateInternetProtocol
        self.fireBaseProtocol = fireBaseProtocol
    }

    //MARK: trazendo as informacoes da api e implementando nos array.
    func getInformation(){
        if internetProtocol.isConnectedToNetwork() {
            apiProtocol.getHeroes(url: apiProtocol.getUrl.requestURL()) { heroes, error in
                if let error = error {
                    self.error = error
                }else{
                    guard let heroesData = heroes?.data.results else {return}
                    self.arrayHeroesHorizontal = Array(heroesData[0...4])
                    self.arrayHeroesVertical   = Array(heroesData[5..<heroesData.count])
                    DataBase.saveCharacterInTheDataBase(heroesData)
                    
                    self.view.reloadData()
                }
            }
        } else {
            self.isConnect = false
            self.view.alertInternetIsNotConect()
            self.populateArrayWithDatabase()
        }
    }
    
    //MARK: funcao do scroll.
    func scroll() {
        var offset = self.arrayHeroesVertical.count
        apiProtocol.getHeroes(url: apiProtocol.getUrl.requestURL()) { response, error in
            if let response = response?.data.results {
                self.arrayHeroesVertical += response
                DataBase.saveCharacterInTheDataBase(response)
            }
            offset+=20
        }
       
        self.view.reloadData()
    }
    
    //MARK: funcao para alimentar o array.
    func getData(){
        arrayHeroesVertical   = Heroes.vertical
        arrayHeroesHorizontal = Heroes.horizontal
    }
    
    //MARK: Funcoes CoreData
    func populateArrayWithDatabase(){
        
        let arrayFromDataBase: [Characters] = DataBase.getdataFromCoreData()
        var character: [Character] = []
        
        for itens in arrayFromDataBase {
            if itens.nameHero != nil{
                var comics: [ComicList] = []
                let comicData = itens.comicHero?.allObjects as? [Datas]
                for comicsDataHero in comicData!{
                    let c = ComicList(resourceURI: comicsDataHero.resource ?? "", name: comicsDataHero.name ?? "")
                    comics.append(c)
                }
                
                let realComics = Comics(items: comics)
                
                var stories: [StorySummary] = []
                let storyData = itens.storyHero?.allObjects as? [Datas]
                for storyDataHero in storyData!{
                    let s = StorySummary(resourceURI: storyDataHero.resource ?? "", name: storyDataHero.name ?? "")
                    stories.append(s)
                }
                
                let realStories = StoryList(items: stories)
                
                let hero: Character = Character(name: itens.nameHero ?? "", thumbnail: Image(path: itens.imageHeroPath ?? "", extension: itens.imageHeroExtension ?? ""), comics: realComics, stories: realStories)

                character.append(hero)
            }
        }
        
        self.arrayHeroesHorizontal = Array(character[0...4])
        self.arrayHeroesVertical   = Array(character[5..<character.count])

        self.view.reloadData()
    }
}
