//
//  Acessibility.swift
//  projetoMarvel
//
//  Created by C94291a on 17/02/22.
//

import Foundation
import UIKit

class Acessibility {
    
    func acessibilityImg(img: ImageViewCell){
        img.isAccessibilityElement = true
        img.accessibilityTraits = .image
    }
    
    func acessibilityCellVertical(cell: VerticalCollectionViewCell){
        cell.isAccessibilityElement = true
        cell.accessibilityLabel = "Name of hero"
        cell.accessibilityHint = "double tap to go to character details"
        cell.accessibilityTraits = .button
    }
    
    func acessibilityCellHorizontal(cell: HorizontalCollectionViewCell){
        cell.isAccessibilityElement = true
        cell.accessibilityLabel = "Name of hero"
        cell.accessibilityHint = "double tap to go to character details"
        cell.accessibilityTraits = .button
    }
}
