//
//  Animacao .swift
//  projetoMarvel
//
//  Created by C94291a on 08/12/21.
//
import UIKit
import Foundation
import Lottie

struct Animation{
    var loading: AnimationView = {
        var animation = AnimationView(name: "loading")
        animation.contentMode = .scaleAspectFit
        animation.loopMode = .loop
        animation.animationSpeed = 2.0
        return animation
    }()
    
    func showLoadingIcon(){
        self.loading.isHidden = false
        self.loading.play()
    }
    func hideLoading(){
        self.loading.isHidden = true
        self.loading.stop()
    }
}
