//
//  imageView.swift
//  projetoMarvel
//
//  Created by C94291a on 22/11/21.
//
import UIKit
import SnapKit
import Kingfisher

class ImageViewCell: UITableViewCell {
    
    var uiiv_Image  = UIImageView()
    var gradient: Bool = false
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(self.uiiv_Image)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Constraints da imagem
    func setImageView(url: URL) {
            self.uiiv_Image.kf.setImage(with: url,
                                        placeholder: UIImage(named: "placeholder"),
                                        options: [
                                            .transition(ImageTransition.fade(2.0)),
                                            .cacheOriginalImage
                                        ],
                                        progressBlock: nil,
                                        completionHandler: nil)
            
            self.uiiv_Image.contentMode = .scaleAspectFit
            self.uiiv_Image.snp.makeConstraints { make in
                make.top.equalTo(self).offset(10)
                make.bottom.equalTo(self).offset(-10)
                make.centerX.equalTo(self)
                make.centerY.equalTo(self)
                make.width.equalTo(415)
                make.height.equalTo(410)
            }
        
        self.uiiv_Image.backgroundColor = .clear
        
    }
    
    //MARK: o gradient do detail
    override func layoutSubviews(){
        if gradient == false{
            let gradientLayer = CAGradientLayer()
            let frameGradiendt = CGRect(x: 0, y: 0, width: 415, height: 410)
            gradientLayer.colors = [UIColor.black.withAlphaComponent(1.0).cgColor,UIColor.black.withAlphaComponent(0.0).cgColor]
            gradientLayer.frame = frameGradiendt
            gradientLayer.startPoint = CGPoint(x: 0.95, y: 0.95)
            gradientLayer.endPoint = CGPoint(x: 0.95, y: 0.0)
            uiiv_Image.layer.insertSublayer(gradientLayer, at: 0)
            gradient = true
        }
    }
}
