//
//  ExtensionsViewController.swift
//  projetoMarvel
//
//  Created by C94291a on 21/11/21.
//
import UIKit
import Kingfisher
import CoreData

//MARK: extension da collection
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionHorizontal{
            return getArrayHorizontal().count
        }
        else {
            return getArrayVertical().count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionHorizontal {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierHorizontal(), for: indexPath) as! HorizontalCollectionViewCell
            cell.lblNome.text = getArrayHorizontal()[indexPath.row].name
            
            
            
            if UIAccessibility.isVoiceOverRunning {
               cell.img.isHidden = true
            }
            
            let path = getArrayHorizontal()[indexPath.row].thumbnail.path
            let imageextension = getArrayHorizontal()[indexPath.row].thumbnail.extension
            let url = "\(path)/standard_fantastic.\(imageextension)"
            let heroUrlImage = URL(string: url)
            
            cell.img.kf.setImage(with: heroUrlImage,
                                 placeholder: UIImage(named: "placeholder"),
                                 options: [
                                    .transition(ImageTransition.fade(2.0)),
                                    .cacheOriginalImage
                                 ],
                                 progressBlock: nil,
                                 completionHandler: nil)
            
            cell.img.layer.cornerRadius  = 25.0
            cell.lblNome.isAccessibilityElement = true
            cell.lblNome.accessibilityLabel = "Name of hero"
            cell.lblNome.accessibilityHint = "double tap to go to character details"
            cell.lblNome.accessibilityTraits = .button
            
            return cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierVertical(), for: indexPath) as! VerticalCollectionViewCell
            cell.lblNome.text = getArrayVertical()[indexPath.row].name
            
            if UIAccessibility.isVoiceOverRunning {
               cell.img.isHidden = true
            }
            let path = getArrayVertical()[indexPath.row].thumbnail.path
            let imageextension = getArrayVertical()[indexPath.row].thumbnail.extension
            let url = "\(path)/standard_fantastic.\(imageextension)"
            let heroUrlImage = URL(string: url)
            
            cell.img.kf.setImage(with: heroUrlImage,
                                 placeholder: UIImage(named: "placeholder"),
                                 options: [
                                    .transition(ImageTransition.fade(2.0)),
                                    .cacheOriginalImage
                                 ],
                                 progressBlock: nil,
                                 completionHandler: nil)
            
           
            cell.img.layer.cornerRadius  = 25.0
            cell.lblNome.isAccessibilityElement = true
            cell.lblNome.accessibilityLabel = "Name of hero"
            cell.lblNome.accessibilityHint = "double tap to go to character details"
            cell.lblNome.accessibilityTraits = .button
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = DetailViewController()
        self.navigationController?.pushViewController(detail, animated: true)
        if  collectionView == collectionVertical {
            detail.self.viewModel.heroTouch = getArrayVertical()[indexPath.row]
            
            let newNameHero = detail.self.viewModel.heroTouch?.name.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
            FireBaseManager().changeTheName(nameHero:  newNameHero ?? "")
        }
        else{
            detail.self.viewModel.heroTouch = getArrayHorizontal()[indexPath.row]
            
            let newNameHero = detail.self.viewModel.heroTouch?.name.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
            FireBaseManager().changeTheName(nameHero:  newNameHero ?? "")
        }
    }
}

//MARK: extension do scrollView
extension ViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = collectionVertical.contentOffset.y
        let endPoint = collectionVertical.contentSize.height
        
        if position > endPoint - scrollView.frame.size.height && pagination() == false && !filter(){
            animationShowLoadingIcon()
            scrol()
        }
        if position < endPoint - scrollView.frame.size.height && pagination() == true || isConnect() == false {
            animationHidden()
        }
    }
}

//MARK: extension da search bar
extension ViewController: UISearchBarDelegate{

    func searchBar(_ searchBar: UISearchBar,
                       textDidChange searchText: String){
        
            var filter = false
        
            guard let searchText = searchBar.text else {return}
            
            if !searchText.isEmpty{
                filter = true
                self.setArrayVertical(heroList: self.getArrayVertical().filter({ heroi in
                    let nomeHeroi = heroi.name
                    return nomeHeroi.contains(searchText)
                }))
                self.collectionVertical.reloadData()
                
            }else{
                filter = false
                DispatchQueue.main.async {
                    self.collectionVertical.reloadData()
                }
                arrayWithDataBase()
            }
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            //TODO: fazer que api traga mais de 20 personagens
            let api = Url()
            let api2 = RealApi()
            var isHidden = animationIsHidden()
            
            guard let searchText = searchBar.text else {return}
            
            let teste = api.requestAPI(name: searchText)
            
            searchBar.resignFirstResponder()
            api2.getHeroes(url: teste) { heroe, error in
                
                guard let response = heroe?.data.results else {return}
                self.setArrayVertical(heroList: response)
                
                DispatchQueue.main.async {
                    self.collectionVertical.reloadData()
                }
                
                if self.getArrayVertical().count > 0 {
                    isHidden = true
                }else{
                    isHidden = false
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Atention", message: "Sorry! The character not found", preferredStyle: .alert)
                        let buttonGoToTheMainScreen = UIAlertAction(title: "Ok", style: .default) { _ in
                            searchBar.text = ""
                            self.arrayWithDataBase()
                        }
                        alert.addAction(buttonGoToTheMainScreen)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
}
