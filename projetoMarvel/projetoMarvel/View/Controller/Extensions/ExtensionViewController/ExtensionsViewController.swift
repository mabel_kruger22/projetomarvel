//
//  ExtensionsViewController.swift
//  projetoMarvel
//
//  Created by C94291a on 21/11/21.
//
import UIKit
import Kingfisher
import CoreData

//MARK: extension da collection
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionHorizontal{
            return getArrayHorizontal().count
        }
        else {
            return getArrayVertical().count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionHorizontal {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierHorizontal(), for: indexPath) as! HorizontalCollectionViewCell
            
            cell.lblNome.text = getArrayHorizontal()[indexPath.row].name
            
            if UIAccessibility.isVoiceOverRunning {
                cell.img.isHidden = true
            }
            
            let path = getArrayHorizontal()[indexPath.row].thumbnail.path
            let imageextension = getArrayHorizontal()[indexPath.row].thumbnail.extension
            let url = "\(path)/standard_fantastic.\(imageextension)"
            let heroUrlImage = URL(string: url)
            
            cell.setImageView(url: heroUrlImage!)
            Acessibility().acessibilityCellHorizontal(cell: cell)
            
            return cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierVertical(), for: indexPath) as! VerticalCollectionViewCell
            cell.lblNome.text = getArrayVertical()[indexPath.row].name
            
            if UIAccessibility.isVoiceOverRunning {
                cell.img.isHidden = true
            }
            
            let path = getArrayVertical()[indexPath.row].thumbnail.path
            let imageextension = getArrayVertical()[indexPath.row].thumbnail.extension
            let url = "\(path)/standard_fantastic.\(imageextension)"
            let heroUrlImage = URL(string: url)
            
            cell.setImageView(url: heroUrlImage!)
            Acessibility().acessibilityCellVertical(cell: cell)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = DetailViewController()
        self.navigationController?.pushViewController(detail, animated: true)
        if  collectionView == collectionVertical {
            detail.setHeroTouch(heroTouch: getArrayVertical()[indexPath.row])
            
            let newNameHero = detail.heroTouch()?.name.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
            FireBaseManager().changeTheName(nameHero:  newNameHero ?? "")
        }
        else{
            detail.setHeroTouch(heroTouch: getArrayHorizontal()[indexPath.row])
            
            let newNameHero = detail.heroTouch()?.name.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
            FireBaseManager().changeTheName(nameHero:  newNameHero ?? "")
        }
    }
}
