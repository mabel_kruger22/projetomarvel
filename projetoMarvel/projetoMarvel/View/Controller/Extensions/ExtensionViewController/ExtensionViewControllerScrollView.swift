//
//  ExtensionViewControllerScrollView.swift
//  projetoMarvel
//
//  Created by C94291a on 16/02/22.
//

import UIKit

//MARK: extension do scrollView
extension ViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = collectionVertical.contentOffset.y
        let endPoint = collectionVertical.contentSize.height
        
        if position > endPoint - scrollView.frame.size.height && pagination() == false && !filter(){
            animationShowLoadingIcon()
            scrol()
        }
        if position < endPoint - scrollView.frame.size.height && pagination() == true || isConnect() == false {
            animationHidden()
        }
    }
}
