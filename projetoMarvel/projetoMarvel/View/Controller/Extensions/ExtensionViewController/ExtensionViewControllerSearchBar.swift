//
//  ExtensionViewControllerSearchBar.swift
//  projetoMarvel
//
//  Created by C94291a on 16/02/22.
//

import UIKit

extension ViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String){
        
        var filterBoolean = filter()
        guard let searchText = searchBar.text else {return}
        
        if !searchText.isEmpty{
            filterBoolean = true
            self.setArrayVertical(heroList: self.getArrayVertical().filter({ heroi in
                let nomeHeroi = heroi.name
                return nomeHeroi.contains(searchText)
            }))
            self.collectionVertical.reloadData()
            
        }else{
            filterBoolean = false
            DispatchQueue.main.async {
                self.collectionVertical.reloadData()
            }
            arrayWithDataBase()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //TODO: fazer que api traga mais de 20 personagens
        let api = Url()
        let api2 = RealApi()
        var isHidden = animationIsHidden()
        
        guard let searchText = searchBar.text else {return}
        
        let teste = api.requestAPI(name: searchText)
        
        searchBar.resignFirstResponder()
        api2.getHeroes(url: teste) { heroe, error in
            
            guard let response = heroe?.data.results else {return}
            self.setArrayVertical(heroList: response)
            
            DispatchQueue.main.async {
                self.collectionVertical.reloadData()
            }
            
            if self.getArrayVertical().count > 0 {
                isHidden = true
            }else{
                isHidden = false
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Atention", message: "Sorry! The character not found", preferredStyle: .alert)
                    let buttonGoToTheMainScreen = UIAlertAction(title: "Ok", style: .default) { _ in
                        searchBar.text = ""
                        self.arrayWithDataBase()
                    }
                    alert.addAction(buttonGoToTheMainScreen)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
