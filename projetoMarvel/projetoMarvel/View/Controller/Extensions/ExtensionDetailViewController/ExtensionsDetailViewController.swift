//
//  ExtensionsDetailViewController.swift
//  projetoMarvel
//
//  Created by C94291a on 22/11/21.
//

import UIKit
import Kingfisher

//MARK: Extension da detailController
extension DetailViewController: UITableViewDelegate, UITableViewDataSource{
    
    //MARK: DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: reuseIdentifier())
        cell.backgroundColor = .black
        
        switch indexPath.row {
        case 0 :
            let cellImage       = ImageViewCell()
            
            guard let urlString = heroTouch()?.thumbnail.path else { return UITableViewCell()}
        
            if !UIAccessibility.isVoiceOverRunning {
                let imageExtension = heroTouch()?.thumbnail.extension
                let url = "\(urlString)/standard_fantastic.\(imageExtension!)"
                let heroUrlImage = URL(string: url)
                
                cellImage.setImageView(url: heroUrlImage!)
                cellImage.selectionStyle = .none
                
                Acessibility().acessibilityImg(img: cellImage)
                
                return cellImage
            }else {
                cellImage.isHidden = true
            }
            
            
        case 1:
            if let name = heroTouch()?.name{
                configNameText(cell: cell)
                cell.detailTextLabel?.text = name
                cell.isAccessibilityElement = true
                cell.accessibilityLabel = "name: \(name)"
            }
            
        case 2:
            guard let comics = heroTouch()?.comics?.items else {return UITableViewCell()}
            configText(cell: cell)
            if comics.count > 0 {
                var names = """
                            """
                for comic in comics {
                    if let namesComics = comic.name{
                        names.append("∙ \(namesComics)\n")
                    }
                }
                cell.textLabel?.text = """
                COMICS:
                
                \(String(describing: names))
                """
           }
        case 3:
            guard let stories = heroTouch()?.comics?.items else {return UITableViewCell()}
            configText(cell: cell)
            if stories.count > 0 {
                var names = """
                            """
                for story in stories {
                    if let storiesName = story.name{
                        names.append("∙\(storiesName)\n")
                    }
                }
                
                cell.textLabel?.text = """
                STORY:
                
                \(String(describing: names))
                """
            }
        default:
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    //MARK: funcoes para a configuracoes dos textLabel e detailTextLabel.
    func configText(cell: UITableViewCell){
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        cell.textLabel?.textColor = .white
        cell.selectionStyle = .none
    }
    func configNameText(cell: UITableViewCell){
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 28.0)
        cell.detailTextLabel?.textColor = .white
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
    }
}
