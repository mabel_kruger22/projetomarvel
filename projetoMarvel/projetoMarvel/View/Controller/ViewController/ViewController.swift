//
//  ViewController.swift
//  projetoMarvel
//
//  Created by C94291a on 19/11/21.
//

import UIKit
import Lottie
import CoreData
import Firebase

class ViewController: UIViewController, ViewControllerProtocol {
  
    private lazy var viewModel: ViewModel = {
        return ViewModel(view: self, dataBaseProtocol: DataBase(), apiProtocol: RealApi(), validateInternetProtocol: InternetConnectionManager(), fireBaseProtocol: FireBaseManager())
    }()
    
    var reloadDataCount: Int = 0
    var alertInternetIsNotConnectCount: Int = 0

    @IBOutlet weak var collectionHorizontal: UICollectionView!
    @IBOutlet weak var collectionVertical  : UICollectionView!
    @IBOutlet weak var searchBarHeroes     : UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionHorizontal.dataSource = self
        collectionHorizontal.delegate   = self
        
        collectionVertical.dataSource   = self
        collectionVertical.delegate     = self
        
        searchBarHeroes.delegate = self
    
        self.viewModel.getData()
        self.viewModel.getInformation()
       
        setupLoading()
        
        FireBaseManager().setUserIDAndUserProperty(propertyUser: "Passion Fruit")
    }
    
    //MARK: para aparecer a navigation.
    override func viewWillAppear(_ animated: Bool) {
        let imgNavigation = UIImageView(image: UIImage(named: "logo1"))
        self.navigationItem.titleView = imgNavigation
        
        FireBaseManager().sendScreenDataToGA(screenName: "Tela Home", screenClass: "ViewController")
    }
    
    func setupLoading(){
        self.viewModel.animation.hideLoading()
        self.viewModel.animation.loading.frame = CGRect(x: self.view.bounds.width/2 - 10, y: 750 , width: 30, height: 30)
        self.view.addSubview(self.viewModel.animation.loading)
    }
    
    func reloadData() {
        collectionVertical.reloadData()
        collectionHorizontal.reloadData()
    }
    
    func alertInternetIsNotConect() {
        let alert = UIAlertController(title: "Attention", message: "You are disconnect of internet", preferredStyle: .alert)
        let buttonCancel = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(buttonCancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getArrayVertical()-> [Character]{
        let arrayVertical = viewModel.arrayHeroesVertical
        return arrayVertical
    }
    
    func setArrayVertical(heroList: [Character]) {
        viewModel.arrayHeroesVertical = heroList
    }
    
    func getArrayHorizontal() -> [Character]{
        let arrayHorizontal = viewModel.arrayHeroesHorizontal
        return arrayHorizontal
    }
    
    func reuseIdentifierHorizontal() -> String{
        let reuseIdentifierHorizontal = viewModel.reuseIdentifier1
        return reuseIdentifierHorizontal
    }
    
    func reuseIdentifierVertical() -> String{
        let reuseIdentifierVertical = viewModel.reuseIdentifier2
        return reuseIdentifierVertical
    }
    
    func pagination() -> Bool {
        let pagination = viewModel.apiProtocol.pagination
        return pagination
    }
    
    func animationHidden() {
        viewModel.animation.hideLoading()
    }
    
    func animationShowLoadingIcon(){
        viewModel.animation.showLoadingIcon()
    }
    
    func animationIsHidden() -> Bool{
        let isHidden = viewModel.isHidden
        return isHidden
    }
    
    func filter() -> Bool{
        let filter = viewModel.filter
        return filter
    }
    
    func isConnect() -> Bool{
        let connect = viewModel.isConnect
        return connect
    }
    
    func scrol(){
        viewModel.scroll()
    }
    
    func arrayWithDataBase(){
        viewModel.populateArrayWithDatabase()
    }
}
