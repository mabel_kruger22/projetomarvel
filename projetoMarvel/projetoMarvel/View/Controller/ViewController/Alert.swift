//
//  Alert.swift
//  projetoMarvel
//
//  Created by C94291a on 14/01/22.
//


extension UIViewController {

    func displayAlert(with title: String, message: String, okText: String = "OK", cancelText: String = "") {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okText, style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))

        if cancelText != "" {
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                self.dismiss(animated: true, completion: nil)
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
}
