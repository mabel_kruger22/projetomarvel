//
//  FireBaseController.swift
//  projetoMarvel
//
//  Created by C94291a on 31/01/22.
//

import Foundation
import Firebase
import UIKit

class FireBaseManager: FireBaseManagerProtocol{
    func sendScreenDataToGA(screenName: String, screenClass: String) {
        Analytics.logEvent(AnalyticsEventScreenView,
                                   parameters: [AnalyticsParameterScreenName: screenName,
                           AnalyticsParameterScreenClass: screenClass])
   }
    
    func changeTheName(nameHero: String){
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
          "hero_name": nameHero
        ])
    }
    
    func setUserIDAndUserProperty(propertyUser food: String){
        Analytics.setUserID("MAB09b39ecca8facad4bafa39af21ebf2269ad78838")
        Analytics.setUserProperty(food, forName: "favorite_food")
    }
}
