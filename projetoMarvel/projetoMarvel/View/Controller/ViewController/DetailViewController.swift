//
//  DetailViewController.swift
//  projetoMarvel
//
//  Created by C94291a on 22/11/21.
//

import UIKit
import Firebase

class DetailViewController: UIViewController{
    
    private lazy var viewModel: DetailViewModel = {
       return DetailViewModel(detailviewModel: self)
    }()

    lazy var detailHero: UITableView = {
        var detail              = UITableView()
        detail.frame            = self.view.bounds
        detail.dataSource       = self
        detail.delegate         = self
        detail.separatorStyle   = .none
        detail.contentInset.bottom = 64
        return detail
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(detailHero)
        self.detailHero.backgroundColor = .black
        self.detailHero.register(ImageViewCell.self, forCellReuseIdentifier: self.viewModel.reuseIdentifier)
    }
    
    //MARK: configuracao navigation
    override func viewWillAppear(_ animated: Bool) {
        let imgNavigation = UIImageView(image: UIImage(named: "logo1"))
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.titleView = imgNavigation
        
        FireBaseManager().sendScreenDataToGA(screenName: "Tela Detalhe", screenClass: "DetailViewController")
    }
    
    func reuseIdentifier()-> String{
        return viewModel.reuseIdentifier
    }

    func heroTouch() -> Character?{
        return viewModel.heroTouch
    }
    
    func setHeroTouch(heroTouch: Character) {
        viewModel.heroTouch = heroTouch
    }
}
