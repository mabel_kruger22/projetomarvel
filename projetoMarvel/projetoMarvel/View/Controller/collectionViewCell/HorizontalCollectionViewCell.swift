//
//  CollectionViewCell.swift
//  projetoMarvel
//
//  Created by C94291a on 21/11/21.
//

import UIKit
import SwiftUI
import Kingfisher

class HorizontalCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    var putGradient: Bool = false
    
    //MARK: Gradient da collection
    override func layoutSubviews(){
        if putGradient == false{
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.black.withAlphaComponent(0.9).cgColor,UIColor.black.withAlphaComponent(0.0).cgColor]
            gradientLayer.frame = img.frame
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
            gradientLayer.cornerRadius = 10
            img.layer.insertSublayer(gradientLayer, at: 0)
            putGradient = true
        }
    }
    
    func setImageView(url: URL) {
            self.img.kf.setImage(with: url,
                                        placeholder: UIImage(named: "placeholder"),
                                        options: [
                                            .transition(ImageTransition.fade(2.0)),
                                            .cacheOriginalImage
                                        ],
                                        progressBlock: nil,
                                        completionHandler: nil)
            
            self.img.contentMode = .scaleAspectFit
            self.img.snp.makeConstraints { make in
                make.top.equalTo(self).offset(0)
                make.bottom.equalTo(self).offset(0)
                make.centerX.equalTo(self)
                make.centerY.equalTo(self)
                make.width.equalTo(180)
                make.height.equalTo(180)
            }
        
        self.img.backgroundColor = .clear
        self.img.layer.cornerRadius  = 25.0
    }
}
