//
//  TestandoTesteMockados.swift
//  projetoMarvelTests
//
//  Created by C94291a on 06/12/21.
//

import XCTest
@testable import projetoMarvel

class ViewModelTest: XCTestCase {
    var sut3 = ViewModel(view: mockViewController(), dataBaseProtocol: mockDataBase(), apiProtocol: mockApi(), validateInternetProtocol: mockValidateInternetIsConnect(), fireBaseProtocol: mockFireBase())

    func testIfReturnSomething(){
        sut3.getInformation()
        
        XCTAssertEqual(sut3.arrayHeroesVertical[0].name, "Abomination (Ultimate)")
        XCTAssertEqual(sut3.arrayHeroesHorizontal[0].name, "3-D Man")
    }
    
    func testReturnDataFromCoreData(){
        sut3.populateArrayWithDatabase()
        
        XCTAssertEqual(sut3.arrayHeroesHorizontal.count, 5)
        XCTAssertTrue(sut3.arrayHeroesVertical.count >= 15)
    }
    
    func testScrollInfity(){
        sut3.scroll()
        
        XCTAssertEqual(sut3.arrayHeroesVertical.count, 20)
    }
    
    func testErroGetInformation(){
        let sut = ViewModel( view: mockViewController(), dataBaseProtocol: mockDataBase(), apiProtocol: mockApiError(), validateInternetProtocol: mockValidateInternetIsConnect(), fireBaseProtocol: mockFireBase())
        sut.getInformation()
        
        guard let error = sut.error else {return}
        XCTAssertEqual(error as! ErrorAPI, ErrorAPI.emptyArray)
    }
    
    func testInternetItsConnect(){
        let conection = sut3.internetProtocol.isConnectedToNetwork()
        XCTAssertEqual(conection, true)
    }
    
    func testInternetIsNotConnect(){
        let sut = ViewModel( view: mockViewController(), dataBaseProtocol: mockDataBase(), apiProtocol: mockApi(), validateInternetProtocol: mockValidateInternetIsNotConnect(), fireBaseProtocol: mockFireBase())
        
        let notConnect = sut.internetProtocol.isConnectedToNetwork()
        XCTAssertEqual(notConnect, false)
    }
    
    func testReloadData(){
        sut3.getInformation()
        
        XCTAssertTrue(sut3.view.reloadDataCount == 1)
    }
    
    func testAlertIternetNotConnect(){
        let sut = ViewModel( view: mockViewController(), dataBaseProtocol: mockDataBase(), apiProtocol: mockApi(), validateInternetProtocol: mockValidateInternetIsNotConnect(), fireBaseProtocol: mockFireBase())
        
        sut.getInformation()
        XCTAssertEqual(sut.view.alertInternetIsNotConnectCount, 1)
    }
}
