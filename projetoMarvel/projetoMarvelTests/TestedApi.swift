//
//  projetoMarvelTests.swift
//  projetoMarvelTests
//
//  Created by C94291a on 02/12/21.
//

import XCTest
@testable import projetoMarvel

class testandoApi: XCTestCase {
    var sut: RealApi!

    override func setUp(){
        self.sut = RealApi()
    }
    
    //verificando se estar retornando o status code 200
    func testApiIfReturnStatusCode200(){
        let expectation = expectation(description: "Sucess")
        sut.getHeroes(url: (sut.getUrl.requestURL())) { response, error in
            XCTAssertEqual(self.sut.statusCode, 200)
            XCTAssertTrue((response?.data.results.count ?? 0) > 0)
            XCTAssertEqual((response?.data.results[0].name), "3-D Man")
            XCTAssertEqual(response?.data.results[0].thumbnail.path, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784")
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3.0)
    }
    
    //verificando o error do catch
    func testApiThrowsEmptyArrayError(){
        let expectation = expectation(description: "Failure")
        let urlNova     = "http://rickandmortyapi.com/api"
        
        sut.getHeroes(url: urlNova) { response, error in
            XCTAssertTrue((error as? ErrorAPI) == ErrorAPI.emptyArray)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3.0)
    }
}

